use std::cmp::Ordering;

use chrono::prelude::*;
use serde::Deserialize;

use super::mirror::Mirror;

#[derive(Deserialize, Debug)]
pub struct MirrorList {
    cutoff: usize,
    last_check: String,
    num_checks: usize,
    check_frequency: usize,
    urls: Vec<Mirror>,
    version: usize,
}

impl MirrorList {
    pub async fn get() -> Result<Self, ()> {
        let mirrorlist: Self;
        match surf::get("https://archlinux.org/mirrors/status/json/").await {
            Ok(mut resp) => match resp.body_json().await {
                Ok(json) => mirrorlist = json,
                Err(e) => panic!("{:?}", e),
            },
            Err(e) => panic!("{:?}", e),
        }
        Ok(mirrorlist)
    }
    pub fn filter_by_country(&mut self, countries: Vec<&str>) {
        let mut mirrorlist = Vec::new();
        for i in 0..self.urls.len() {
            for country in countries.iter() {
                if &self.urls[i].country == country {
                    mirrorlist.push(self.urls[i].clone());
                }
            }
        }
        self.urls = mirrorlist;
    }
    pub fn print_mirrorlist(&self) {
        println!("# Last Update: {:?}\n", Utc::now().to_rfc2822());
        for mirror in self.urls.iter() {
            println!("Server = {}", mirror.url);
        }
    }
    pub fn sort_by_completion_pct(&mut self) {
        self.urls
            .sort_by(|a, b| a.completion_pct.total_cmp(&b.completion_pct));
    }
    pub fn sort_by_delay(&mut self) {
        self.urls.sort_by(|a, b| {
            if a.delay.is_none() && b.delay.is_none() {
                Ordering::Equal
            } else if a.delay.is_none() && b.delay.is_some() {
                Ordering::Less
            } else if a.delay.is_some() && b.delay.is_none() {
                Ordering::Greater
            } else {
                a.delay.unwrap().total_cmp(&b.delay.unwrap())
            }
        });
    }
    pub fn sort_by_duration_avg(&mut self) {
        self.urls.sort_by(|a, b| {
            if a.duration_avg.is_none() && b.duration_avg.is_none() {
                Ordering::Equal
            } else if a.duration_avg.is_none() && b.duration_avg.is_some() {
                Ordering::Less
            } else if a.duration_avg.is_some() && b.duration_avg.is_none() {
                Ordering::Greater
            } else {
                a.duration_avg.unwrap().total_cmp(&b.duration_avg.unwrap())
            }
        });
    }
    pub fn sort_by_duration_stddev(&mut self) {
        self.urls.sort_by(|a, b| {
            if a.duration_stddev.is_none() && b.duration_stddev.is_none() {
                Ordering::Equal
            } else if a.duration_stddev.is_none() && b.duration_stddev.is_some() {
                Ordering::Less
            } else if a.duration_stddev.is_some() && b.duration_stddev.is_none() {
                Ordering::Greater
            } else {
                a.duration_stddev
                    .unwrap()
                    .total_cmp(&b.duration_stddev.unwrap())
            }
        });
    }
    pub fn sort_by_score(&mut self) {
        self.urls.sort_by(|a, b| {
            if a.score.is_none() && b.score.is_none() {
                Ordering::Equal
            } else if a.score.is_none() && b.score.is_some() {
                Ordering::Less
            } else if a.score.is_some() && b.score.is_none() {
                Ordering::Greater
            } else {
                a.score.unwrap().total_cmp(&b.score.unwrap())
            }
        });
    }
}
