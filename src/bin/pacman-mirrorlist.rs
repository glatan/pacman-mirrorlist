use clap::{App, Arg};
use pacman_mirrorlist::MirrorList;

#[async_std::main]
async fn main() {
    let matches = App::new("pacman-mirrorlist")
        .name(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .about("mirrorlist generator for pacman")
        .args(&[
            Arg::with_name("country")
                .short("c")
                .long("country")
                .value_name("COUNTRY NAME")
                .multiple(true),
            Arg::with_name("sort-by")
                .short("s")
                .long("sort-by")
                .takes_value(true)
                .help("Chose one from [delay, duration-avg, duration-stddev, score]."),
            Arg::with_name("overwrite")
                .long("overwrite")
                .help("Overwrite existing \"/etc/pacman.d/mirrorlist.\""),
        ])
        .get_matches();
    let mut mirrorlist = MirrorList::get().await.unwrap();
    if let Some(c) = matches.values_of("country") {
        mirrorlist.filter_by_country(c.collect());
    }
    if let Some(s) = matches.value_of("sort-by") {
        match s {
            "delay" => mirrorlist.sort_by_delay(),
            "duration_avg" => mirrorlist.sort_by_duration_avg(),
            "duration_stddev" => mirrorlist.sort_by_duration_stddev(),
            "score" => mirrorlist.sort_by_score(),
            _ => (),
        }
    }
    if matches.is_present("overwrite") {
        unimplemented!();
    } else {
        mirrorlist.print_mirrorlist();
    }
}
