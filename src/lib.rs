#![feature(total_cmp)] // https://github.com/rust-lang/rust/issues/72599

mod mirror;
mod mirrorlist;

pub use mirrorlist::MirrorList;
