use serde::Deserialize;

#[derive(Clone, Deserialize, Debug)]
pub(crate) struct Mirror {
    pub(crate) url: String,
    pub(crate) protocol: String,
    pub(crate) last_sync: Option<String>,
    pub(crate) completion_pct: f64,
    pub(crate) delay: Option<f64>,
    pub(crate) duration_avg: Option<f64>,
    pub(crate) duration_stddev: Option<f64>,
    pub(crate) score: Option<f64>,
    pub(crate) active: bool,
    pub(crate) country: String,
    pub(crate) country_code: String,
    pub(crate) isos: bool,
    pub(crate) ipv4: bool,
    pub(crate) ipv6: bool,
    pub(crate) details: String,
}
